**GIT**

GitLab is a complete DevOps platform, delivered as a single application. This makes GitLab unique and creates a streamlined software workflow, unlocking your organization from the constraints of a pieced together toolchain

![GIT](https://miro.medium.com/max/4400/1*oMC83-7fB27k1tTMxDfRaQ.png)


**USES OF GIT**
There are a number of reasons.. The first is that it enables slick and easy collaboration and version control. This allows you to work on code with anyone from anywhere. Additionally, many employers use GitHub. So, if you plan on getting a job, you’ll look really good if you already know your way around GitHub. And don’t forget about the connections, learning, and portfolio aspects. GitHub is a robust learning and collaboration platform. Take time to explore it and see just how much it can expand your programming knowledge.

![GIT](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQONnkJIOQznyYGhaXwWV5k49HVQTw4kFTa2Q&usqp=CAU)