Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

![D](https://www.cloudmantra.net/wp-content/uploads/2018/04/download_1_1_400x260.png)

**Docker Engine**
Docker Engine is a client-server application with these major components


![D](https://docs.docker.com/engine/images/engine-components-flow.png)


**USES OF Docker**

![D](https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/10/Docker-Use-Cases-01.jpg)

